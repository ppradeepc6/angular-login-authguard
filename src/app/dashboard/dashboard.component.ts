import { Component, OnInit } from '@angular/core';
import { AuthService } from '../service/auth.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {
  updateUserInfo:any;
  constructor(private authService:AuthService) { 

  }

  ngOnInit(): void {
    this.authService.getNewUserInfo().subscribe(info => {
      this.updateUserInfo = info;
    })
  }
}
