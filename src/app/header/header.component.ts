import { Component, OnInit } from '@angular/core';
import { AuthService } from '../service/auth.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
  hideAndShowBtn:boolean=false
  constructor(private authService:AuthService) { }

  ngOnInit(): void {
  }

  openNav() {
    document.getElementById("mySidenav").style.width = "250px";
    this.hideAndShowBtn=true;
  }

  closeNav() {
    this.hideAndShowBtn=false;
    document.getElementById("mySidenav").style.width = "0";
  }

  logOut(){
    this.authService.logout()
  }
}
