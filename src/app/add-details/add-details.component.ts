import { Component, OnInit} from '@angular/core';
import { AuthService } from '../service/auth.service';
import { FormControl, Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';

@Component({
  selector: 'app-add-details',
  templateUrl: './add-details.component.html',
  styleUrls: ['./add-details.component.css']
})
export class AddDetailsComponent implements OnInit {
  firstNameFormControl = new FormControl('', [Validators.required]);
  emailFormControl = new FormControl('', [Validators.required]);
  lastNameFormControl = new FormControl('', [Validators.required]);
  constructor(
    private authService:AuthService,
    private toast: ToastrService,
    private router:Router) { }

  ngOnInit() {
  }

  submitData() {
    if(this.firstNameFormControl.errors || this.lastNameFormControl.errors
      || this.emailFormControl.errors){
        this.toast.error(`Filled required fields`);
      return;
    }

    this.authService.setNewUserInfo({
      firstName: this.firstNameFormControl.value,
      email: this.emailFormControl.value,
      lastName: this.lastNameFormControl.value,
    });
    this.toast.success(`${this.firstNameFormControl.value} successFully added`);
    this.router.navigate(["/dashboard"]);
  }
}
