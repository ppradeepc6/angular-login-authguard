import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { BehaviorSubject } from 'rxjs';

@Injectable({
    providedIn: 'root'
})
export class AuthService {
    UserDetails: any = {
        username: "pradeep",
        password: 123456,
        email: "ppradeepc6@gmail.com",
        firstName: "Pradeep",
        lastName: "Choudhary"
    }

    private newUser = new BehaviorSubject<any>(this.UserDetails);
    constructor(private router: Router) {
    }

    login(username: string, password: string) {
        if (this.UserDetails.username == username && this.UserDetails.password == password) {
            return true
        } else {
            return false;
        }
    }

    logout() {
        // remove user from local storage to log user out
        localStorage.removeItem('islogin');
        this.router.navigate(["login"])
    }

    setNewUserInfo(user: any) {
        this.newUser.next(user);
    }

    getNewUserInfo() {
        return this.newUser.asObservable();
    }
}
