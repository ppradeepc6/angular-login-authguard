import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AddDetailsComponent } from './add-details.component';
import { AddDetailsRoutingModule } from './add-details-routing.module';
import { MaterialModule } from '../material/material.module';

@NgModule({
  declarations: [AddDetailsComponent
  ],
  imports: [
    CommonModule,
    AddDetailsRoutingModule,
    MaterialModule
  ]
})
export class AddDetailsModule { }
