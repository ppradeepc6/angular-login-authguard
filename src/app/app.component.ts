import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'angular-simple-login';

  constructor(private router :Router) {
    if (localStorage.islogin) {
      this.router.navigate(["/dashboard"]);
    } else {
      this.router.navigateByUrl("/login");
    }
  }

  ngOnInit(): void {
  }

}
