import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { AuthService } from './auth.service';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {

  routeURL: string;
  constructor(
    private router: Router,
    private authService: AuthService
  ) { }
  // canActivate(
  //   next: ActivatedRouteSnapshot,
  //   state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
  //   return true;
  // }
  canActivate(next: ActivatedRouteSnapshot, state: RouterStateSnapshot): Promise<boolean> {
    let self = this
    // here we check if user is logged in or not
    return new Promise((resolve, reject) => {
      //this.authdataService.fireAuth.onAuthStateChanged((user) => {
      const localUserId = localStorage.islogin;
      //const localUserData = this.localService.localUserData;
      if (!localUserId && this.routeURL !== '/login') {
        this.routeURL = '/login';
        // when the user is not logged in,
        // instead of just returning false
        this.router.navigate(['/login'], {
          // note: this queryParams returns the current URL
          // that we can have in 'return' parameter,
          // so when the '/login' page opens,
          // this param tell us from where it comes
          // read-more to understand better👇👇
          queryParams: {
            return: state.url
          }
        });
        return resolve(false);
      }
      else {
        //self.menuCtrl.enable(true);
        //self.menuCtrl.swipeGesture(true);
        this.routeURL = this.router.url;
        // just return true - if user is logged in
        return resolve(true);
      }
      //});
    });
  }
}
